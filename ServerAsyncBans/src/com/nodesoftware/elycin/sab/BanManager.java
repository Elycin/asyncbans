package com.nodesoftware.elycin.sab;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import org.bukkit.Bukkit;

public class BanManager {

	private ArrayList<String> aryStrings;

	public static String driver = "";
	public static String database = "";
	public static String hostname = "";
	public static String port = "";
	public static String playerbanstable = "";
	public static String username = "";
	public static String password = "";
	public static String banstable = "";
	public BanManager() {

	}

	public Connection connect() {
		Connection theConnection = null;
		try {
			theConnection = DriverManager.getConnection(driver + "://"+hostname+":"+port+"/"+database, username, password);
		} catch (SQLException e) {
			Bukkit.getLogger().log(Level.SEVERE, "ServerAsyncBans has encountered an SQL Error: " + e.toString());
			Bukkit.shutdown();
		}
		return theConnection;
	}

	public void disconnect(Connection con) {
		try {
			con.close();
		} catch (SQLException e) {
			Bukkit.getLogger().log(Level.SEVERE, "ServerAsyncBans has encountered an SQL Error: " + e.toString());
			Bukkit.shutdown();
		}
	}

	public BanInfo checkIfBanned(String username) {
		Connection con = connect();
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM "+banstable.toString()+" WHERE player = ? LIMIT 1");
			stmt.setString(1, username);
			ResultSet banInfo = stmt.executeQuery();
			if (banInfo.next()) {
				String reason = banInfo.getString("reason");
				disconnect(con);
				
				return new BanInfo(true, username, reason);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		disconnect(con);
		return new BanInfo(false, username);
	}
	
	public void insertBanInfo (BanInfo bi) {
		if(bi.isBanned()){
			String username = bi.getUsername();
			String reason = bi.getReason();
			Connection con = connect();
			try {
				PreparedStatement stmt = con.prepareStatement("INSERT INTO "+banstable.toString()+" (player, reason) VALUES (?, ?)");
				stmt.setString(1, username);
				stmt.setString(2, reason);
				ResultSet banInfo = stmt.executeQuery();
				if (banInfo.next()) {
					
					disconnect(con);

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			disconnect(con);
		}
	}

	public static class BanInfo {
		private boolean isBanned;
		private String username;
		private String reason;

		public BanInfo(boolean isBanned, String username) {
			this(isBanned, username, "Reason Unknown");
		}

		public BanInfo(boolean isBanned, String username, String reason) {
			this.isBanned = isBanned;
			this.username = username;
			this.reason = reason;
		}

		public boolean isBanned() {
			return this.isBanned;
		}

		public String getUsername() {
			return this.username;
		}

		public String getReason() {
			return this.reason;
		}
	}

}
