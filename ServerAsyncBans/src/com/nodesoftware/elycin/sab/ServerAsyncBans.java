package com.nodesoftware.elycin.sab;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * This is our entry point class.
 * 
 * @author Elycin
 * @authors notrodash
 */
public class ServerAsyncBans extends JavaPlugin implements Listener {
	File configFile = new File(getDataFolder() + File.separator + "config.yml");
	private static ServerAsyncBans instance;
	private CommandManager commandManager;
	private BanManager banManager;
	
	public static String CV = "";
	@Override
	public void onEnable() {
		instance = this;
		commandManager = new CommandManager();
		banManager = new BanManager();
		Bukkit.getPluginManager().registerEvents(this, this);
		PluginDescriptionFile pdf = this.getDescription();
		
		CV = pdf.getVersion().toString();
			BanManager.driver = this.getConfig().getString("driver");
			BanManager.hostname = this.getConfig().getString("hostname");
			BanManager.port = this.getConfig().getString("port");
			BanManager.database = this.getConfig().getString("database");
			BanManager.username = this.getConfig().getString("username");
			BanManager.password = this.getConfig().getString("password");
			BanManager.banstable = this.getConfig().getString("PlayerBans");
		
		this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		//Analytics
		 try {
			Statistics.ReportToNodeSoftware();
		} catch (IOException e) {
			this.getLogger().log(Level.INFO, "Failed to post statistics");
			e.printStackTrace();
		}
	}

	@Override
	public void onDisable() {
		this.getLogger().log(Level.INFO, "ServerAsyncBans has been disabled");
	}

	public static ServerAsyncBans getInstance() {
		return instance;
	}

	public BanManager getBanManager() {
		return this.banManager;
	}
	    
	}


