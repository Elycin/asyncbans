package com.nodesoftware.elycin.sab;

import java.util.Map;
import java.util.logging.Level;

import org.bukkit.Bukkit;

public class CommandManager {

	public CommandManager(){
		Map<String, Map<String, Object>> cmds = ServerAsyncBans.getInstance().getDescription().getCommands();
		for(String command:cmds.keySet()){
			Class<?> baseCommandClass;
			try {
				baseCommandClass = CommandManager.class.getClassLoader().loadClass("com.nodesoftware.elycin.sab.commands." + command);
				BaseCommand baseCommand = (BaseCommand)baseCommandClass.newInstance();
				
				ServerAsyncBans.getInstance().getCommand(command).setExecutor(baseCommand);
			} catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
				Bukkit.getLogger().log(Level.INFO, "No class found to execute command: " + command);
				e.printStackTrace();
			}
		}
	}
	
}
