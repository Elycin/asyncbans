package com.nodesoftware.elycin.sab.commands;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.nodesoftware.elycin.sab.BanManager;
import com.nodesoftware.elycin.sab.BanManager.BanInfo;
import com.nodesoftware.elycin.sab.BaseCommand;
import com.nodesoftware.elycin.sab.ServerAsyncBans;

public class asyncban extends BaseCommand {

	public boolean onCommand(CommandSender sender, Command cmd,
			String label, String[] args) {
		boolean isAllowed = (sender instanceof Player ? ((Player) sender)
				.isOp() : (sender instanceof ConsoleCommandSender)); // Short-hand if statements. Returns true if console or op.
		if (isAllowed) {
			BanManager banManager = ServerAsyncBans.getInstance()
					.getBanManager();
			Player target = sender.getServer().getPlayer(args[0]);
			String reason = args[1];
			// see if player is online
			if (target == null) {
				banManager.insertBanInfo(new BanInfo(true, args[0], reason));
				return true;
			}
			banManager.insertBanInfo(new BanInfo(true, args[0], reason));
			target.kickPlayer(reason);

		}
		return false;
	}

}
