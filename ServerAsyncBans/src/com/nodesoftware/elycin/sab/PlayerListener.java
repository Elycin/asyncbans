package com.nodesoftware.elycin.sab;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.nodesoftware.elycin.sab.BanManager.BanInfo;
import com.nodesoftware.update.AUCore;

public class PlayerListener implements Listener {
	private AUCore core;
	Logger log = Logger.getLogger("Minecraft");
	@EventHandler
	
	
	public void onPlayerJoin(PlayerJoinEvent event) {
		BanInfo bi = ServerAsyncBans.getInstance().getBanManager().checkIfBanned(event.getPlayer().getName());
		if (bi.isBanned()) {
			event.getPlayer().kickPlayer(bi.getReason());
		}
		//updater
		core = new AUCore("http://nodesoftware.com/Bukkit/com/nodesoftware/elycin/sab/version.txt", log, "[AsyncBans]");
		double currentVer = 1.1, currentSubVer = 0;

		if(!core.checkVersion(currentVer, currentSubVer, "AsyncBans")){
	event.getPlayer().sendMessage(ChatColor.RED+"[AsyncBans]Critical Update Released.");
		}
		
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		/*
		 * Irrelevant for the ban purpose alone, however very useful when
		 * handling custom player instances
		 */
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		BanManager banManager = ServerAsyncBans.getInstance().getBanManager();
	    if(cmd.getName().equalsIgnoreCase("asyncban")){
	        Player target = sender.getServer().getPlayer(args[0]);
	        String reason = args[1];
	         // see if player is online
	        
	        if (target == null) {
	        	banManager.insertBanInfo(new BanInfo(true, args[0], reason));
	            return true;
	        }
	        banManager.insertBanInfo(new BanInfo(true, args[0], reason));
	        target.kickPlayer(reason);
	        
	    }
	    return false;
	}

}
