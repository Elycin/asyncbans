package com.nodesoftware.elycin.sab;//back,so could you explain this more to me?

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class BaseCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player)
			return onPlayerCommand((Player) sender, cmd, label, args);
		if (sender instanceof BlockCommandSender)
			return onCommandBlockCommand((BlockCommandSender) sender, cmd,
					label, args);
		return onConsoleCommand((ConsoleCommandSender) sender, cmd, label, args);
	}

	// Abstraction for various senders, convenience functions. These functions are not abstract so they aren't mandatory to override.
	public boolean onPlayerCommand(Player sender, Command cmd, String label,
			String[] args) {
		return false;
	}

	public boolean onConsoleCommand(ConsoleCommandSender sender, Command cmd,
			String label, String[] args) {
		return false;
	}

	public boolean onCommandBlockCommand(BlockCommandSender sender,
			Command cmd, String label, String[] args) {
		return false;
	}

}
